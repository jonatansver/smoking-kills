/**
 * Created by jonat on 9/27/2016.
 */


(function(angular){
    "use strict";
    angular.module('SmokingKills',[])
        .controller('MainController',['$scope', 'NicotineLiquidSolution',
            'NicotineLiquid', MainController]);
    function MainController($scope, NicotineLiquidSolution, NicotineLiquid){
        var ctrl = this;
        $scope.solution = new NicotineLiquidSolution;
        $scope.solution.nicotineLevel = undefined;

        $scope.addLiquidToSolution = function(solution){
            solution.components.push(new NicotineLiquid());
            console.log($scope.solution)
        };

        $scope.toggleSolutionAndLiquid = function(parent, componentIndex){
            if(parent.components[componentIndex].name === 'Solution'){
                parent.components[componentIndex] = new NicotineLiquid();
            }
            else if(parent.components[componentIndex].name === 'Liquid'){
                parent.components[componentIndex] = new NicotineLiquidSolution();
                parent.components[componentIndex].nicotineLevel = undefined;
            }
        };

        $scope.calculate = function(){
            var gaussMatrix = [[0.3,0,0,0.2],[1,1,1,1],[0,1,0,0.3]];
            console.log(gauss(gaussMatrix))
        };

        var calculateSolutionProportions = function(solution){


        };


        $scope.percentToProportion = function(percent){
            if(percent && percent != ''){
                return percent / 100;
            }
            else {
                return undefined;
            }
        };

        $scope.proportionToPercent = function(proportion){
            if(proportion && proportion != ''){
                return (proportion * 100).toFixed(2);
            }
            else{
                return undefined;
            }
        }
    }

})(angular);
