/**
 * Created by jonat on 9/30/2016.
 */
(function(angular){
    "use strict";
    angular.module('SmokingKills')
        .factory('NicotineLiquidSolution', ['Solution', 'NicotineLiquid', NicotineLiquidSolutionFactory]);

    function NicotineLiquidSolutionFactory(SolutionFactory, NicotineLiquid){
        var NicotineLiquidSolution = function(){
            var solution = new SolutionFactory('Solution');
            solution.components.push(new NicotineLiquid());
            solution.components.push(new NicotineLiquid());
            return solution;
        };
        return NicotineLiquidSolution;
    }

})(angular);

