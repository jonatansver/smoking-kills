/**
 * Created by jonat on 9/30/2016.
 */
(function(angular){
    "use strict";
    angular.module('SmokingKills')
        .factory('NicotineLiquid', ['Solution', 'Nicotine', NicotineLiquidFactory]);

    function NicotineLiquidFactory(SolutionFactory, Nicotine){
        var NicotineLiquid = function(){
            var liquid = new SolutionFactory('Liquid');
            liquid.components.push(new Nicotine());
            return liquid;
        };
        return NicotineLiquid;
    }

})(angular);