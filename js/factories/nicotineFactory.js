/**
 * Created by jonat on 9/30/2016.
 */
(function(angular){
    "use strict";
    angular.module('SmokingKills')
        .factory('Nicotine', ['Solution', NicotineFactory]);

    function NicotineFactory(SolutionFactory){
        var Nicotine = function(){
            return new SolutionFactory('Nicotine');
        };
        return Nicotine;
    }

})(angular);