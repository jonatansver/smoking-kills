/**
 * Created by jonat on 9/28/2016.
 */

(function(angular){
    "use strict";
    angular.module('SmokingKills')
        .factory('Solution', [SolutionFactory]);

    function SolutionFactory(){
        var Solution = function(name){
            this.components =[];
            this.name = name;
        };
        Solution.prototype.setComponentProportion = function (index, proportion) {
            this.components[index].proportion = undefined;

            if(proportion <= this.getNewComponentMaximumProportion()){
                this.components[index].proportion = proportion;
            }
        };
        Solution.prototype.getNewComponentMaximumProportion = function () {
            var currentSum = 0;
            this.components.forEach(function (component) {
                    if (component.proportion) {
                        currentSum += component.proportion;
                    }
                }
            );
            return 1 - currentSum;
        };
        return Solution;

    }

})(angular);