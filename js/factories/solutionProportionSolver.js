/**
 * Created by jonat on 10/4/2016.
 */
(function(angular){
    "use strict";
    angular.module('SmokingKills')
        .factory('SolutionProportionSolver', SolutionProportionSolverFactory);

    function SolutionProportionSolverFactory(){
        var Solver = function(solution, expectedNicotineLevel){
            var unknownProportionsCount = 0;
            var unknownNicotineLevelCount = 0;
            var sumKnownProportions = 0;
            var sumKnownNicotineLevel = 0;
            for(var i = 0; i < solution.components.length; i++){
                if(solution.components[i].proportion){
                    sumKnownProportions += solution.components[i].proportion;
                }
                else{
                    unknownProportionsCount++;
                }
            }

            if(unknownProportionsCount === 1){
                solution.components[unknownProportions[0]].proportion = 1 - sumKnownProportions;
            }

            if(solution.nicotineLevel && unknownProportions.length === 2){
                var gaussMatrix = [];
                gaussMatrix.push([1,1,1-sumKnownProportions]);
                gaussMatrix.push()
            }
        };

        var assignNicotineLevels = function(solution, solutionNicotineLevel){
            solution.components.forEach(function(component){
                if(component.name === 'Liquid'){
                    component.nicotineLevel = component.components[0].proportion;
                }
                else if(component.name === 'Solution'){
                }
            })
        };

        return Solver;
    }

})(angular);